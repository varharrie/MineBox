﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineBoxRelease.Core;

namespace MineBoxRelease.Model
{
    public class Music : NotificationObject
    {
        private string title;
        private string path;
        private string artist;
        private string album;
        private TimeSpan duration;

        public string TitleStr
        {
            get { return Title + " - " + Artist; }
        }
        public TimeSpan Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                this.RaisePropertyChanged("Duration");
            }
        }

        public String DurationStr
        {
            get { return string.Format("{0:D2}:{1:D2}", duration.Minutes, duration.Seconds); }
        }
        public string Album
        {
            get { return album; }
            set
            {
                album = value;
                this.RaisePropertyChanged("Album");
            }
        }


        public string Artist
        {
            get { return artist; }
            set
            {
                artist = value;
                this.RaisePropertyChanged("Artist");
            }
        }

        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                this.RaisePropertyChanged("Path");
            }
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                this.RaisePropertyChanged("Title");
            }
        }

    }
}
