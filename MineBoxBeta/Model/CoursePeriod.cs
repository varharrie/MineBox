﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineBoxRelease.Core;

namespace MineBoxRelease.Model
{
    public enum WeekType
    {
        All,
        //奇数（单周）
        Odd,
        //偶数（双周）
        Even
        //全部
    }

    public class CoursePeriod : NotificationObject
    {//星期几
        private int day;
        //第几节
        private int period;
        //开始周
        private int startWeek;
        //结束周
        private int endWeek;
        //单双周类型
        private WeekType weekType;
        //地点
        private string site;

        public CoursePeriod()
        {

        }

        public CoursePeriod(int day, int period, int startWeek, int endWeek, WeekType weekType, string site)
        {
            this.Day = day;
            this.Period = period;
            this.StartWeek = startWeek;
            this.EndWeek = endWeek;
            this.WeekType = weekType;
            this.Site = site;
        }

        public bool Conform(int week, int day, int period)
        {
            bool flag = (weekType == WeekType.All) || (weekType == WeekType.Odd && (week % 2 == 1)) || (weekType == WeekType.Even && (week % 2 == 0));
            return flag && (this.startWeek <= week && this.endWeek >= week) && this.day == day && this.period == period;
        }

        public CoursePeriod(string str, string site)
        {
            //获取day
            Debug.WriteLine(str);
            switch (str[1])
            {
                case '一': day = 1; break;
                case '二': day = 2; break;
                case '三': day = 3; break;
                case '四': day = 4; break;
                case '五': day = 5; break;
                case '六': day = 6; break;
                case '七': day = 7; break;
                case '日': day = 7; break;
            }

            string ii = str[3].ToString();
            //获取time
            period = (int.Parse(str[3].ToString()) - 1) / 2;

            //获取week
            string weekStr = str.Substring(str.IndexOf('{') + 1);
            weekStr = weekStr.Substring(0, weekStr.Length - 1);
            string[] weekNum = weekStr.Substring(1, weekStr.IndexOf('周') - 1).Split('-');
            startWeek = int.Parse(weekNum[0]);
            endWeek = int.Parse(weekNum[1]);
            if (weekStr.IndexOf('|') > 0)
                weekType = weekStr[weekStr.IndexOf('|') + 1].Equals('单') ? WeekType.Odd : WeekType.Even;
            else
                weekType = WeekType.All;

            this.site = site;
        }

        public string Site
        {
            get { return site; }
            set
            {
                site = value;
                this.RaisePropertyChanged("Site");
            }
        }

        public WeekType WeekType
        {
            get { return weekType; }
            set
            {
                weekType = value;
                this.RaisePropertyChanged("WeekType");
            }
        }


        public int EndWeek
        {
            get { return endWeek; }
            set
            {
                endWeek = value;
                this.RaisePropertyChanged("EndWeek");
            }
        }


        public int StartWeek
        {
            get { return startWeek; }
            set
            {
                startWeek = value;
                this.RaisePropertyChanged("StartWeek");
            }
        }


        public int Period
        {
            get { return period; }
            set
            {
                period = value;
                this.RaisePropertyChanged("Period");
            }
        }


        public int Day
        {
            get { return day; }
            set
            {
                day = value;
                this.RaisePropertyChanged("Day");
            }
        }

    }
}
