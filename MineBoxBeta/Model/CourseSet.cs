﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineBoxRelease.Model
{
    public class CourseSet : List<Course>
    {
        public Course this[string courseName]
        {
            get { return this.FirstOrDefault(model => model.Name.Equals(courseName)); }
            set
            {
                Course course = this.FirstOrDefault(model => model.Name.Equals(courseName));
                if (course != null)
                    this.Remove(course);
                base.Add(course);
            }
        }

        public void AddNotRepeat(Course course)
        {
            Course item = this[course.Name];
            if (item == null) { base.Add(course); return; }
            foreach (CoursePeriod period in course.Periods)
            {
                item.Periods.Add(period);
            }
        }

        public Course GetCourse(int week, int day, int period)
        {
            Course course = null;
            Boolean flag = false;
            foreach (Course c in this)
            {
                foreach (CoursePeriod p in c.Periods)
                {
                    if (p.Conform(week, day, period))
                    {
                        course = new Course();
                        course.CourseColor = c.CourseColor;
                        course.Name = c.Name;
                        course.TeacherName = c.TeacherName;
                        course.Periods.Add(p);
                        flag = true;
                    }
                }
                if (flag) break;
            }
            return course;
        }

    }
}
