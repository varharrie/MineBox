﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineBoxRelease.Core;

namespace MineBoxRelease.Model
{
    //快捷方式数据存储类
    public class Shortcut:NotificationObject
    {
        private string title;
        private string filePath;
        public Shortcut()
        {

        }

        public Shortcut(string title, string path)
        {
            Title = title;
            FilePath = path;
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                this.RaisePropertyChanged("Title");
            }
        }

        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                this.RaisePropertyChanged("FilePath");
            }
        }

    }
}
