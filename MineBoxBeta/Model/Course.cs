﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;
using MineBoxRelease.Core;

namespace MineBoxRelease.Model
{
    public class Course : NotificationObject
    {
        private string name;
        private string teacherName;

        private List<CoursePeriod> periods;

        private Color courseColor;

        public Course()
        {
            name = "null";
            periods = new List<CoursePeriod>();
        }

        public Course(string str)
        {
            str = str.Trim();
            string[] strings = Regex.Split(str, "<br />");
            name = strings[0].Trim();
            teacherName = strings[2].Trim();
            if (teacherName.IndexOf('(') > 0)
                teacherName = teacherName.Substring(0, teacherName.IndexOf('('));
            periods = new List<CoursePeriod>();
            periods.Add(new CoursePeriod(strings[1].Trim(), strings[3].Trim()));
        }

        public override string ToString()
        {
            string str = name;
            foreach (CoursePeriod model in periods)
            {
                str += " | 周" + model.Day + " 第" + model.Period + "节 {" + model.StartWeek + "-" + model.EndWeek + " " +
                (model.WeekType == WeekType.All ? "全" : model.WeekType == WeekType.Odd ? "单" : "双") + "} " + model.Site;
            }
            return str;
        }

        public Color CourseColor
        {
            get { return courseColor; }
            set
            {
                courseColor = value;
                this.RaisePropertyChanged("CourseColor");
            }
        }

        public List<CoursePeriod> Periods
        {
            get { return periods; }
            set
            {
                periods = value;
                this.RaisePropertyChanged("Periods");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                this.RaisePropertyChanged("Name");
            }
        }
        public string TeacherName
        {
            get { return teacherName; }
            set
            {
                teacherName = value;
                this.RaisePropertyChanged("TeacherName");
            }
        }
    }
}
