﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineBoxRelease.Core;

namespace MineBoxRelease.Model
{
    public class Plan:NotificationObject
    {
        private string content;
        private bool isCompleted;

        public bool IsCompleted
        {
            get { return isCompleted; }
            set
            {
                isCompleted = value;
                this.RaisePropertyChanged("IsCompleted");
            }
        }
        
        public string Content
        {
            get { return content; }
            set
            {
                content = value;
                this.RaisePropertyChanged("Content");
            }
        }

        public Plan()
        {
            IsCompleted = false;
            Content = "";
        }
    }
}
