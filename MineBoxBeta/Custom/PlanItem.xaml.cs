﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Model;

namespace MineBoxRelease.Custom
{
    /// <summary>
    /// PlanItem.xaml 的交互逻辑
    /// </summary>
    public partial class PlanItem : UserControl
    {
        //注册事件
        public static readonly RoutedEvent CheckChangedEvent =
            EventManager.RegisterRoutedEvent("CheckChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanItem));
        //定义事件处理器属性
        public event RoutedEventHandler CheckChanged
        {
            add { AddHandler(CheckChangedEvent, value); }
            remove { RemoveHandler(CheckChangedEvent, value); }
        }

        //注册事件
        public static readonly RoutedEvent EnterAcceptedEvent =
            EventManager.RegisterRoutedEvent("EnterAccepted", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanItem));
        //定义事件处理器属性
        public event RoutedEventHandler EnterAccepted
        {
            add { AddHandler(EnterAcceptedEvent, value); }
            remove { RemoveHandler(EnterAcceptedEvent, value); }
        }

        //注册事件
        public static readonly RoutedEvent InputFinishededEvent =
            EventManager.RegisterRoutedEvent("InputFinisheded", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanItem));
        //定义事件处理器属性
        public event RoutedEventHandler InputFinisheded
        {
            add { AddHandler(InputFinishededEvent, value); }
            remove { RemoveHandler(InputFinishededEvent, value); }
        }

        //注册事件
        public static readonly RoutedEvent ClosingEvent =
            EventManager.RegisterRoutedEvent("Closing", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PlanItem));
        //定义事件处理器属性
        public event RoutedEventHandler Closing
        {
            add { AddHandler(ClosingEvent, value); }
            remove { RemoveHandler(ClosingEvent, value); }
        }

        public Plan Plan { get; set; }

        public PlanItem(Plan plan)
        {
            InitializeComponent();
            Plan = plan;
            this.DataContext = Plan;
            cbFlag.Click += (s, e) => RaiseEvent(new RoutedEventArgs(CheckChangedEvent));
            tbContent.PreviewKeyUp += tbContent_PreviewKeyUp;
            tbContent.LostFocus += (s, e) => RaiseEvent(new RoutedEventArgs(InputFinishededEvent));
            this.MouseEnter += (s, e) => btnClose.Visibility = Visibility.Visible;
            this.MouseLeave += (s, e) => btnClose.Visibility = Visibility.Collapsed;
            btnClose.Click += (s, e) => RaiseEvent(new RoutedEventArgs(ClosingEvent));
        }

        void tbContent_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Debug.WriteLine("enter!");
                RaiseEvent(new RoutedEventArgs(EnterAcceptedEvent));
            }
        }

        public void DoFocus()
        {
            this.Focus();
            tbContent.Focus();
        }
    }
}
