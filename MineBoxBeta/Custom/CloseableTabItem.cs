﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MineBoxRelease.View;

namespace MineBoxRelease.Custom
{
    [TemplatePart(Name = "btnClose", Type = typeof(Button))]
    public class CloseableTabItem : DefaultTabItem
    {
        //注册事件
        public static readonly RoutedEvent TabClosingEvent =
            EventManager.RegisterRoutedEvent("TabClosing", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CloseableTabItem));
        //定义事件处理器属性
        public event RoutedEventHandler TabClosing
        {
            add { AddHandler(TabClosingEvent, value); }
            remove { RemoveHandler(TabClosingEvent, value); }
        }

        public CloseableTabItem()
        {
        }

        private Button btnClose;
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            btnClose = GetTemplateChild("btnClose") as Button;
            if (btnClose == null) return;
            //触发关闭事件
            btnClose.Click += (s, e) => RaiseEvent(new RoutedEventArgs(TabClosingEvent, this));
        }
    }
}
