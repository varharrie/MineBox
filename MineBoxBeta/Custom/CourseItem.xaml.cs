﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Model;

namespace MineBoxRelease.Custom
{
    /// <summary>
    /// CourseItem.xaml 的交互逻辑
    /// </summary>
    public partial class CourseItem : UserControl
    {
        public string CourseName { get; set; }
        public string CourseInfo { get; set; }
        public CourseItem(Course coModel = null)
        {
            InitializeComponent();
            if (coModel == null) return;
            border.Background = new SolidColorBrush(coModel.CourseColor);
            CoursePeriod peModel = coModel.Periods[0];
            CourseName = coModel.Name;
            CourseInfo = coModel.TeacherName + " | " + peModel.Site;
            DataContext = this;
        }
    }
}
