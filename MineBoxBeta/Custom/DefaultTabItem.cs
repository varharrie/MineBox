﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MineBoxRelease.View;

namespace MineBoxRelease.Custom
{
    public class DefaultTabItem : RadioButton
    {
        //注册事件
        public static readonly RoutedEvent ViewChangedEvent =
            EventManager.RegisterRoutedEvent("ViewChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DefaultTabItem));

        //定义事件处理器属性
        public event RoutedEventHandler ViewChanged
        {
            add { AddHandler(ViewChangedEvent, value); }
            remove { RemoveHandler(ViewChangedEvent, value); }
        }

        private BaseView view;
        public BaseView View
        {
            get
            {
                return view;
            }
            set
            {
                view = value;
                view.Tab = this;
                this.ToolTip = view.Title;
                RaiseEvent(new RoutedEventArgs(ViewChangedEvent, this));
            }
        }

        public DefaultTabItem()
        {
        }
    }
}
