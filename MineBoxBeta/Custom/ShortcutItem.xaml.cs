﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Model;

namespace MineBoxRelease.Custom
{
    /// <summary>
    /// ShortcutItem.xaml 的交互逻辑
    /// </summary>
    public partial class ShortcutItem : UserControl
    {

        //注册事件
        public static readonly RoutedEvent RemovingEvent =
            EventManager.RegisterRoutedEvent("Removing", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ShortcutItem));

        public event RoutedEventHandler Removing
        {
            add { base.AddHandler(RemovingEvent, value); }
            remove { base.RemoveHandler(RemovingEvent, value); }
        }

        public Shortcut Data { get; set; }

        public static readonly DependencyProperty EditableProperty = DependencyProperty.Register("Editable", typeof(Boolean), typeof(ShortcutItem));
        public Boolean Editable
        {
            get { return (Boolean)GetValue(EditableProperty); }
            set
            {
                SetValue(EditableProperty, value);
                if (btnRemove == null) return;
                btnRemove.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public ShortcutItem()
        {
            InitializeComponent();
            Editable = false;
        }

        public ShortcutItem(Shortcut shortcut)
        {
            InitializeComponent();
            Editable = false;
            Data = shortcut;
            DataContext = Data;
        }

        private void BtnRemove_OnClick(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(RemovingEvent, this));
        }
    }
}
