﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MineBoxRelease.Core;
using MineBoxRelease.Model;

namespace MineBoxRelease.Win
{
    /// <summary>
    /// ScheduleImportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ScheduleImportWindow : Window
    {
        public string UserName { get; set; }
        public string Password { set; get; }
        public string CheckCode { get; set; }
        public CourseSet Courses { get; set; }

        private ZFSystem zfs;
        public ScheduleImportWindow()
        {
            InitializeComponent();
            InitializeData();
            InitializeEvent();
        }

        private void InitializeData()
        {
            zfs = new ZFSystem("http://10.1.1.96");
            img.Source = zfs.GetCheckCode();
            tbMsg.Text = "课程表信息将从教务系统导入\r\n您的账号密码仅用于系统登陆";
        }

        //初始化事件
        private void InitializeEvent()
        {
            MouseLeftButtonDown += (s, e) => DragMove();
            btnImport.Click += btnImport_Click;
            btnCancel.Click += (s, e) => { this.DialogResult = false; this.Close(); };
            img.PreviewMouseUp += (s, e) => img.Source = zfs.GetCheckCode();
        }

        void btnImport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string loginResult = zfs.Login(tbUserName.Text, tbPassword.Password, tbCheckCode.Text);
                if (loginResult != null)
                {
                    img.Source = zfs.GetCheckCode();
                    MessageBox.Show(loginResult);
                    return;
                }
                string html = zfs.GetSchedulePageHtml(tbUserName.Text);
                ZFHtmlParser parser = new ZFHtmlParser(html);
                Courses = parser.GetSchedule();
                this.DialogResult = true;
                this.Close();
            }
            catch (Exception)
            {
                this.DialogResult = false;
            }
        }
    }
}
