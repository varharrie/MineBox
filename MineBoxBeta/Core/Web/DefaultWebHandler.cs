﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MineBoxRelease.Core.Web
{
    class DefaultWebHandler : IWebHandler
    {
        private HttpWebRequest request;
        private CookieContainer cookieContainer = new CookieContainer();
        private string referer;
        public DefaultWebHandler(string refererStr = null)
        {
            if (refererStr != null) referer = refererStr;
            // cookieContainer=new CookieContainer();
        }

        //创建Request
        public HttpWebRequest CreateRequest(string url, SubmitMethod method)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Referer = referer;
            if (cookieContainer == null || cookieContainer.Count == 0)
                cookieContainer = new CookieContainer();
            req.CookieContainer = cookieContainer;
            switch (method)
            {
                case SubmitMethod.Get:
                    req.Method = "GET";
                    break;
                case SubmitMethod.Post:
                    req.Method = "POST";
                    break;
            }


            return req;

        }

        //使用Post提交
        public string Post(string url, string dataStr, Encoding encoding = null)
        {
            request = CreateRequest(url, SubmitMethod.Post);
            request.ContentType = "application/x-www-form-urlencoded";
            if (encoding == null) encoding = Encoding.UTF8;
            byte[] dataBytes = encoding.GetBytes(dataStr);
            request.ContentLength = dataBytes.Length;
            using (Stream stream = request.GetRequestStream())
            {
                stream.Write(dataBytes, 0, dataBytes.Length);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //if (response.Cookies != null) cookieContainer.Add(response.Cookies);
            String result = null;
            if (response.GetResponseStream() != null)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding("gb2312")))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;
        }

        //使用Get提交
        public string Get(string url, string dataStr = "")
        {
            if (dataStr != null && !dataStr.Equals("")) dataStr = "?" + dataStr;
            request = CreateRequest(url + dataStr, SubmitMethod.Get);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //if (response.Cookies != null) cookieContainer.Add(response.Cookies);
            String result = null;
            if (response.GetResponseStream() != null)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding("gb2312")))
                {
                    result = sr.ReadToEnd();
                }
            }
            return result;
        }

        //获取图片，转化为byte[]
        public byte[] GetImgToBytes(string url)
        {
            request = CreateRequest(url, SubmitMethod.Get);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //if (response.Cookies != null) cookieContainer.Add(response.Cookies);
            byte[] bytes = null;
            if (response.GetResponseStream() != null)
            {
                bytes = new byte[response.ContentLength];
                using (Stream stream = response.GetResponseStream())
                {
                    stream.Read(bytes, 0, bytes.Length);
                }
            }
            return bytes;
        }

        //获取图片，转化为BitMapImage
        public ImageSource GetImgToBitmapImage(string url)
        {
            byte[] bytes = GetImgToBytes(url);
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.StreamSource = new MemoryStream(bytes);
            bitmap.EndInit();
            return bitmap;
        }
    }
}
