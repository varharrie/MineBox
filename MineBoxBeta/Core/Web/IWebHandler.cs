﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MineBoxRelease.Core.Web
{
    enum SubmitMethod { Post, Get }
    interface IWebHandler
    {
        string Post(string url, string dataStr, Encoding encoding = null);
        string Get(string url, string dataStr);
        ImageSource GetImgToBitmapImage(string url);
    }
}
