﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MineBoxRelease.View;

namespace MineBoxRelease.Core.View
{
    interface IViewCreator
    {
        BaseView Create(XmlElement root);
        BaseView Create(string title = null);
        void Save(BaseView view,string path);
    }
}
