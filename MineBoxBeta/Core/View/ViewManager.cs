﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MineBoxRelease.View;

namespace MineBoxRelease.Core.View
{
    class ViewManager
    {
        #region 视图注册表管理

        //视图注册表
        private static List<RegeditItem> regedit;
        public static List<RegeditItem> Regedit
        {
            get { return regedit ?? (regedit = new List<RegeditItem>()); }
        }
        //注册视图
        public static void RegisterView(String viewName, Type viewType, String viewIcon, Type creatorType)
        {
            RegeditItem itemOld = GetRegeditItemByName(viewName);
            if (itemOld != null) return;
            var item = new RegeditItem
            {
                ViewName = viewName,
                ViewType = viewType,
                ViewIcon = viewIcon,
                CreatorType = creatorType,
            };
            Regedit.Add(item);
        }

        #endregion

        //通过注册的视图名，获取注册项目信息
        public static RegeditItem GetRegeditItemByName(String viewName)
        {
            return Regedit.FirstOrDefault(item => item.ViewName.Equals(viewName));
        }

        public static RegeditItem GetRegeditItemByType(Type type)
        {
            return Regedit.FirstOrDefault(item => item.ViewType == type);
        }

        //通过视图名称，获取视图生成器
        public static IViewCreator PrepareFormViewName(String viewName)
        {
            IViewCreator creator;
            try
            {
                Type creatorType = GetRegeditItemByName(viewName).CreatorType;
                creator = Activator.CreateInstance(creatorType) as IViewCreator;
            }
            catch (Exception)
            {
                return null;
            }
            return creator;
        }

        public static IViewCreator PrepareFormViewType(Type type)
        {
            IViewCreator creator;
            try
            {
                Type creatorType = GetRegeditItemByType(type).CreatorType;
                creator = Activator.CreateInstance(creatorType) as IViewCreator;
            }
            catch (Exception)
            {
                return null;
            }
            return creator;
        }

        //通过视图名称，创建视图
        public static BaseView CreateView(String viewName, String title = null)
        {
            return PrepareFormViewName(viewName).Create(title);
        }

        //通过xml，创建视图
        public static BaseView CreateViewFromXml(String xmlPath)
        {
            BaseView view;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlPath);
                XmlElement root = doc.DocumentElement;
                if (root == null) throw new Exception("找不到根节点！");
                string viewName = root.Attributes["type"].Value;
                IViewCreator creator = PrepareFormViewName(viewName);
                view = creator.Create(root);
            }
            catch (Exception)
            {
                return null;
            }
            return view;
        }

        public static void Save(BaseView view)
        {
            Type type = view.GetType();
            string path = ViewXmlConfiguration.ApplyPath(view);
            PrepareFormViewType(type).Save(view, path);
        }
    }
}
