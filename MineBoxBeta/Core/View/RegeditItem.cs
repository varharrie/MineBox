﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MineBoxRelease.Core.View
{
    //视图注册表项目
    class RegeditItem
    {
        //视图名称（唯一）
        public String ViewName { get; set; }
        //视图类型
        public Type ViewType { get; set; }
        //视图图标
        public String ViewIcon { get; set; }
        //视图生成器类型
        public Type CreatorType { get; set; }
    }
}
