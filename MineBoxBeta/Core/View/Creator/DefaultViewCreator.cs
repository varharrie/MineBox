﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MineBoxRelease.View;

namespace MineBoxRelease.Core.View.Creator
{
    class DefaultViewCreator : IViewCreator
    {
        public BaseView Create(XmlElement root)
        {
            throw new NotImplementedException();
        }

        public BaseView Create(string title = null)
        {
            if (title == null) title = "新建";
            return new DefaultView { Title = title };
        }

        public void Save(BaseView view,string path)
        {
            throw new NotImplementedException();
        }
    }
}
