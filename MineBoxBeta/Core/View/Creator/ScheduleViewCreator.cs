﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml;
using MineBoxRelease.Model;
using MineBoxRelease.View;

namespace MineBoxRelease.Core.View.Creator
{
    class ScheduleViewCreator : IViewCreator
    {
        public BaseView Create(XmlElement root)
        {
            ScheduleView view;
            try
            {
                string tabTitle = root.GetAttribute("title");
                int curWeek = int.Parse(root.GetAttribute("curWeek"));
                DateTime date = DateTime.ParseExact(root.GetAttribute("date"), "yyyy-MM-dd",
                    System.Globalization.CultureInfo.CurrentCulture);
                TimeSpan span = DateTime.Now - date;
                int day = (int)date.DayOfWeek;
                if (day == 0) day = 7;
                curWeek += (span.Days + day - 1) / 7;
                XmlNodeList nodes = root.GetElementsByTagName("course");
                CourseSet courses = new CourseSet();
                foreach (XmlNode node in nodes)
                {
                    Course course = new Course();
                    course.Name = node["name"].InnerText;
                    course.TeacherName = node["teacherName"].InnerText;
                    course.CourseColor = (Color)ColorConverter.ConvertFromString(node["color"].InnerText);
                    course.Periods = new List<CoursePeriod>();
                    XmlElement ele = node as XmlElement;
                    XmlNodeList periodNodes = ele.GetElementsByTagName("period");
                    foreach (XmlNode periodNode in periodNodes)
                    {
                        CoursePeriod period = new CoursePeriod();
                        period.Day = int.Parse(periodNode.Attributes["day"].InnerText);
                        period.Period = int.Parse(periodNode.Attributes["period"].InnerText);
                        period.StartWeek = int.Parse(periodNode.Attributes["startWeek"].InnerText);
                        period.EndWeek = int.Parse(periodNode.Attributes["endWeek"].InnerText);
                        string weekType = periodNode.Attributes["weekType"].InnerText;
                        period.WeekType = weekType.Equals("All")
                            ? WeekType.All
                            : (weekType.Equals("Odd") ? WeekType.Odd : WeekType.Even);
                        period.Site = periodNode.Attributes["site"].InnerText;
                        course.Periods.Add(period);
                    }
                    courses.Add(course);
                }
                view = new ScheduleView(tabTitle);
                view.CurWeek = curWeek;
                view.Courses = courses;
                view.GetWeekCourses(curWeek);
            }
            catch (Exception)
            {
                throw;
            }
            return view;
        }

        public BaseView Create(string title = null)
        {
            if (title == null || title.Equals("")) title = "课程表";
            return new ScheduleView(title);
        }

        public void Save(BaseView view, string path)
        {
            try
            {
                ScheduleView v = view as ScheduleView;
                CourseSet courses = v.Courses;
                int curWeek = v.CurWeek;
                XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();
                /*根节点*/
                writer.WriteStartElement("viewxml");
                writer.WriteAttributeString("title", v.Title);
                writer.WriteAttributeString("type", "Schedule");
                writer.WriteAttributeString("curWeek", curWeek.ToString());
                writer.WriteAttributeString("date", DateTime.Now.ToString("yyyy-MM-dd"));
                foreach (Course course in courses)
                {
                    writer.WriteStartElement("course");
                    writer.WriteElementString("name", course.Name);
                    writer.WriteElementString("color", course.CourseColor.ToString());
                    writer.WriteElementString("teacherName", course.TeacherName);
                    foreach (CoursePeriod period in course.Periods)
                    {
                        writer.WriteStartElement("period");
                        writer.WriteAttributeString("day", period.Day.ToString());
                        writer.WriteAttributeString("period", period.Period.ToString());
                        writer.WriteAttributeString("startWeek", period.StartWeek.ToString());
                        writer.WriteAttributeString("endWeek", period.EndWeek.ToString());
                        writer.WriteAttributeString("weekType", period.WeekType.ToString());
                        writer.WriteAttributeString("site", period.Site);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
