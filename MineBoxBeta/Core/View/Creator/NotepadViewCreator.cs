﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MineBoxRelease.View;

namespace MineBoxRelease.Core.View.Creator
{
    class NotepadViewCreator : IViewCreator
    {
        public BaseView Create(XmlElement root)
        {
            NotepadView view;
            try
            {
                string tabTitle = root.GetAttribute("title");

                string content = root.GetElementsByTagName("content")[0].InnerText;

                view = new NotepadView(tabTitle);
                view.NotepadContent = content;
            }
            catch (Exception)
            {
                throw;
            }
            return view;
        }

        public BaseView Create(string title = null)
        {
            if (title == null || title.Equals("")) title = "记事本";
            return new NotepadView(title);
        }

        public void Save(BaseView view, string path)
        {
            try
            {
                NotepadView v = view as NotepadView;
                string content = v.NotepadContent;
                XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();
                /*根节点*/
                writer.WriteStartElement("viewxml");
                writer.WriteAttributeString("title", v.Title);
                writer.WriteAttributeString("type", "Notepad");

                writer.WriteElementString("content", content);

                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
