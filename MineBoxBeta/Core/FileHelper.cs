﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using IWshRuntimeLibrary;

namespace MineBoxRelease.Core
{
    class FileHelper
    {//获取图标
        public static BitmapSource GetIcon(string filePath)
        {
            System.Drawing.Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(filePath);
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(icon.Handle,
                new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
        }

        public static String GetActualFilePath(String srcPath)
        {
            String filePath = srcPath;
            string fileEx = System.IO.Path.GetExtension(srcPath);
            switch (fileEx)
            {
                case "": return null;             //文件夹
                case ".url": return null;        //网页
                case ".lnk":                //快捷方式
                    filePath = GetTargetPath(filePath);
                    break;
            }
            return filePath;
        }

        public static string GetTargetPath(string filePath)
        {
            IWshShortcut curShortcut = (IWshShortcut)new WshShellClass().CreateShortcut(filePath);
            return curShortcut.TargetPath;
        }

        public static string GetFileName(string filePath)
        {
            return Path.GetFileNameWithoutExtension(filePath);
        }
    }
}
