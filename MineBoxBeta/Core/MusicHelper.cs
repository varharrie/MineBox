﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MineBoxRelease.Model;
using Shell32;

namespace MineBoxRelease.Core
{
    class MusicHelper
    {
        public static Music MusicFromFile(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                    throw new Exception("文件不存在！");
                Music music = new Music();
                ShellClass sh = new ShellClass();
                Folder dir = sh.NameSpace(Path.GetDirectoryName(filePath));
                FolderItem file = dir.ParseName((Path.GetFileName(filePath)));
                string title = dir.GetDetailsOf(file, 21);
                string artist = dir.GetDetailsOf(file, 13);
                string album = dir.GetDetailsOf(file, 14);
                string duration = dir.GetDetailsOf(file, 27);
                Debug.WriteLine("标题：" + title);
                Debug.WriteLine("歌手：" + artist);
                Debug.WriteLine("专辑：" + album);
                Debug.WriteLine("时长：" + duration);

                music.Title = title;
                music.Artist = artist;
                music.Album = album;
                music.Duration = TimeSpan.Parse(duration);
                music.Path = filePath;

                return music;
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }

    }
}
