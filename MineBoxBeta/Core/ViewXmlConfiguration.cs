﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MineBoxRelease.View;

namespace MineBoxRelease.Core
{
    //管理view的id和配置文件的路径
    class ViewXmlConfiguration : Dictionary<int, String>
    {
        private ViewXmlConfiguration() { }
        public const String Path = "viewconfig.xml";
        private const String xmlPathFormat = "xml/{0}.viewxml";
        private static ViewXmlConfiguration data;

        //单例实现
        public static ViewXmlConfiguration Data
        {
            get
            {
                if (data == null) Load();
                return data;
            }
        }

        private static int maxId;

        public static int MaxId { get { return maxId; } }

        //加载/初始化配置
        public static void Load()
        {
            data = new ViewXmlConfiguration();
            maxId = 0;
            if (File.Exists(Path))
            {
                Debug.WriteLine("Configuration loading...");
                XmlDocument doc = new XmlDocument();
                doc.Load(Path);
                XmlElement root = doc.DocumentElement;
                maxId = int.Parse(root.GetAttribute("maxid"));
                XmlNodeList nodes = root.GetElementsByTagName("viewmxl");
                foreach (XmlNode node in nodes)
                {
                    int id = int.Parse(node.Attributes["id"].InnerText);
                    string path = node.Attributes["path"].InnerText;
                    data.Add(id, path);
                    if (id > maxId) maxId = id;
                }
                return;
            }
            Debug.WriteLine("Configuration initializing...");

        }

        //保存配置
        public static void Save()
        {
            if (data == null) return;
            Debug.WriteLine("Configuration saving...");
            XmlTextWriter writer = new XmlTextWriter(Path, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("configuration");
            writer.WriteAttributeString("maxid", maxId.ToString());
            foreach (int id in data.Keys)
            {
                writer.WriteStartElement("viewmxl");
                writer.WriteAttributeString("id", id.ToString());
                writer.WriteAttributeString("path", data[id]);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }

        //查找已有路径或生成新路径
        public static string ApplyPath(BaseView view)
        {
            if (Data.ContainsKey(view.Id) && view.Id > -1) return data[view.Id];
            maxId++;
            Debug.WriteLine("maxId=" + maxId);
            view.Id = maxId;
            String path = string.Format(xmlPathFormat, view.Id);
            Data.Add(view.Id, path);
            if (!Directory.Exists("xml")) Directory.CreateDirectory("xml");
            Save();
            return path;
        }

        //移除配置
        public static void Remove(BaseView view)
        {
            File.Delete(string.Format(xmlPathFormat, view.Id));
            Data.Remove(view.Id);
            if (Data.Count == 0) maxId = 0;
            Save();
        }

        public override string ToString()
        {
            if (data == null) return "";
            string str = "";
            foreach (int id in data.Keys)
            {
                str += id + ":" + data[id] + "\r\n";
            }
            return str;
        }
    }
}
