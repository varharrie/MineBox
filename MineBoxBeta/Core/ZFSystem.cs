﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using MineBoxRelease.Core.Web;

namespace MineBoxRelease.Core
{
    //http://10.1.1.96/CheckCode.aspx
    //http://10.1.1.96/default2.aspx
    //http://10.1.1.96/xskbcx.aspx
    class ZFSystem
    {
        private IWebHandler webHandler;
        private string rootPath;

        public ZFSystem(string rootPath)
        {
            this.rootPath = rootPath;
            webHandler = new DefaultWebHandler(rootPath);
        }

        //登陆验证（成功返回null，异常返回非空字符串）
        public String Login(string userName, string password, string checkCode)
        {
            string viewstate = "dDwyODE2NTM0OTg7Oz7c6%2B5UiE0x3F6KGVy0J%2Fg7vA1NIQ==";
            string txtUserName = userName;
            string textBox2 = password;
            string txtSecretCode = checkCode;
            string radioButtonList1 = "学生";
            string button1 = "";
            string lbLanguage = "";
            string hidPdrs = "";
            string hidsc = "";

            String dataStr = string.Format(
                "__VIEWSTATE={0}&txtUserName={1}&TextBox2={2}&txtSecretCode={3}&RadioButtonList1={4}&Button1={5}&lbLanguage={6}&hidPdrs={7}&hidsc={8}"
                , viewstate
                , txtUserName
                , textBox2
                , txtSecretCode
                , radioButtonList1
                , button1
                , lbLanguage
                , hidPdrs
                , hidsc);
            string result = webHandler.Post(rootPath + "/default2.aspx", dataStr);
            if (result == null) return "未知错误！";
            //Debug.WriteLine(result);
            string info = new ZFHtmlParser(result).GetLoginResult();
            return info;
        }

        //课程表页面html
        public string GetSchedulePageHtml(string xh)
        {
            //&xm=%E9%BB%84%E6%96%87%E7%9A%93
            string result = webHandler.Get(rootPath + "/xskbcx.aspx?xh=" + xh + "&gnmkdm=N121603", "");
            if (result == null) return null;
            //Debug.WriteLine(result);
            return result;
        }

        public string GetSchedulePageHtml(string xh, int grade, int term)
        {
            string eventtarget = "";
            string eventargument = "";
            string viewstate =
                "dDwzOTI4ODU2MjU7dDw7bDxpPDE+Oz47bDx0PDtsPGk8MT47aTwyPjtpPDQ+O2k8Nz47aTw5PjtpPDExPjtpPDEzPjtpPDE1PjtpPDI0PjtpPDI2PjtpPDI4PjtpPDMwPjtpPDMyPjtpPDM0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcZTs+Pjs+Ozs+O3Q8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDx4bjt4bjs+Pjs+O3Q8aTwzPjtAPDIwMTQtMjAxNTsyMDEzLTIwMTQ7MjAxMi0yMDEzOz47QDwyMDE0LTIwMTU7MjAxMy0yMDE0OzIwMTItMjAxMzs+PjtsPGk8MT47Pj47Oz47dDx0PDs7bDxpPDA+Oz4+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85a2m5Y+377yaMjAxMjMwMDg5MDUxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlp5PlkI3vvJrpu4TmlofnmpM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWtpumZou+8muiuoeeul+acuuW3peeoi+WtpumZojs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85LiT5Lia77ya6K6h566X5py656eR5a2m5LiO5oqA5pyvOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzooYzmlL/nj63vvJoxMuiuoeeul+acuuenkeWtpuS4juaKgOacrzHnj607Pj47Pjs7Pjt0PDtsPGk8MT47PjtsPHQ8QDA8Ozs7Ozs7Ozs7Oz47Oz47Pj47dDxwPGw8VmlzaWJsZTs+O2w8bzxmPjs+PjtsPGk8MT47PjtsPHQ8QDA8Ozs7Ozs7Ozs7Oz47Oz47Pj47dDxAMDxwPHA8bDxQYWdlQ291bnQ7XyFJdGVtQ291bnQ7XyFEYXRhU291cmNlSXRlbUNvdW50O0RhdGFLZXlzOz47bDxpPDE+O2k8MD47aTwwPjtsPD47Pj47Pjs7Ozs7Ozs7Ozs+Ozs+O3Q8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDM+O2k8Mz47bDw+Oz4+Oz47Ozs7Ozs7Ozs7PjtsPGk8MD47PjtsPHQ8O2w8aTwxPjtpPDI+O2k8Mz47PjtsPHQ8O2w8aTwwPjtpPDE+O2k8Mj47aTwzPjtpPDQ+O2k8NT47aTw2Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDzpq5jnuqfor63oqIDnqIvluo/orr7orqHlpKfkvZzkuJo7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOW7lueOsjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MS4wOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyMC0yMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Jm5ic3BcOzs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85LiN5o6S6K++Oz4+Oz47Oz47Pj47dDw7bDxpPDA+O2k8MT47aTwyPjtpPDM+O2k8ND47aTw1PjtpPDY+Oz47bDx0PHA8cDxsPFRleHQ7PjtsPOaAneaUv+ivvuekvuS8muWunui3tTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8546L6ZW/57qiOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE5LTIwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzkuI3mjpLor747Pj47Pjs7Pjs+Pjt0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47PjtsPHQ8cDxwPGw8VGV4dDs+O2w86YeR5bel5a6e5LmgOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlt6Xorq3kuK3lv4M7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDIuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTAtMTE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOS4jeaOkuivvjs+Pjs+Ozs+Oz4+Oz4+Oz4+O3Q8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDA+O2k8MD47bDw+Oz4+Oz47Ozs7Ozs7Ozs7Pjs7Pjt0PEAwPHA8cDxsPFBhZ2VDb3VudDtfIUl0ZW1Db3VudDtfIURhdGFTb3VyY2VJdGVtQ291bnQ7RGF0YUtleXM7PjtsPGk8MT47aTwzPjtpPDM+O2w8Pjs+Pjs+Ozs7Ozs7Ozs7Oz47bDxpPDA+Oz47bDx0PDtsPGk8MT47aTwyPjtpPDM+Oz47bDx0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDEzLTIwMTQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOmHkeW3peWunuS5oDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85bel6K6t5Lit5b+DOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjs+Pjt0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDEzLTIwMTQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOmrmOe6p+ivreiogOeoi+W6j+iuvuiuoeWkp+S9nOS4mjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85buW546yOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxLjA7Pj47Pjs7Pjs+Pjt0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDEzLTIwMTQ7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOaAneaUv+ivvuekvuS8muWunui3tTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8546L6ZW/57qiOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjs+Pjs+Pjs+Pjs+Pjs+Pjs+KrsdCR9rv3ZyB1Y4u6wUPolhsjY=";
            //string viewstate = "dDwzOTI4ODU2MjU7dDw7bDxpPDE+Oz47bDx0PDtsPGk8MT47aTwyPjtpPDQ+O2k8Nz47aTw5PjtpPDExPjtpPDEzPjtpPDE1PjtpPDI0PjtpPDI2PjtpPDI4PjtpPDMwPjtpPDMyPjtpPDM0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDxcZTs+Pjs+Ozs+O3Q8dDxwPHA8bDxEYXRhVGV4dEZpZWxkO0RhdGFWYWx1ZUZpZWxkOz47bDx4bjt4bjs+Pjs+O3Q8aTwzPjtAPDIwMTQtMjAxNTsyMDEzLTIwMTQ7MjAxMi0yMDEzOz47QDwyMDE0LTIwMTU7MjAxMy0yMDE0OzIwMTItMjAxMzs+PjtsPGk8MD47Pj47Oz47dDx0PDs7bDxpPDE+Oz4+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85a2m5Y+377yaMjAxMjMwMDg5MDUxOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzlp5PlkI3vvJrpu4TmlofnmpM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWtpumZou+8muiuoeeul+acuuW3peeoi+WtpumZojs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85LiT5Lia77ya6K6h566X5py656eR5a2m5LiO5oqA5pyvOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzooYzmlL/nj63vvJoxMuiuoeeul+acuuenkeWtpuS4juaKgOacrzHnj607Pj47Pjs7Pjt0PDtsPGk8MT47PjtsPHQ8QDA8Ozs7Ozs7Ozs7Oz47Oz47Pj47dDxwPGw8VmlzaWJsZTs+O2w8bzxmPjs+PjtsPGk8MT47PjtsPHQ8QDA8Ozs7Ozs7Ozs7Oz47Oz47Pj47dDxAMDxwPHA8bDxQYWdlQ291bnQ7XyFJdGVtQ291bnQ7XyFEYXRhU291cmNlSXRlbUNvdW50O0RhdGFLZXlzOz47bDxpPDE+O2k8MT47aTwxPjtsPD47Pj47Pjs7Ozs7Ozs7Ozs+O2w8aTwwPjs+O2w8dDw7bDxpPDE+Oz47bDx0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47aTw3Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDEyMDA4Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyMDEyMDA4Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzosIMwMzIyOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwoMjAxNC0yMDE1LTIpLTkxMTEwMS0yMDEyMDA4LTE7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOWkp+WtpueUn+WwseS4muaMh+WvvDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGoNeesrDXoioLov57nu60y6IqCe+esrDgtOOWRqH0vQTUtMjA2L+Wnmua3keWnrDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85ZGoMeesrDnoioLov57nu60y6IqCe+esrDgtOOWRqOWPjOWRqH0vQTUtMjA2L+Wnmua3keWnrDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MjAxNS0wNC0yMS0xNy0zNjs+Pjs+Ozs+Oz4+Oz4+Oz4+O3Q8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDI+O2k8Mj47bDw+Oz4+Oz47Ozs7Ozs7Ozs7PjtsPGk8MD47PjtsPHQ8O2w8aTwxPjtpPDI+Oz47bDx0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47PjtsPHQ8cDxwPGw8VGV4dDs+O2w8QVNQLm5ldOe9kee7nOe8lueoi+mhueebruWunui3tTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85buW546yOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwyLjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDE5LTIwOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwmbmJzcFw7Oz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzkuI3mjpLor747Pj47Pjs7Pjs+Pjt0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0PjtpPDU+O2k8Nj47PjtsPHQ8cDxwPGw8VGV4dDs+O2w86Z2Z5oCB572R56uZ6K6+6K6hOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDzmnLHmnJ3lubM7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDEuMDs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8MTktMjA7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPCZuYnNwXDs7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOS4jeaOkuivvjs+Pjs+Ozs+Oz4+Oz4+Oz4+O3Q8QDA8cDxwPGw8UGFnZUNvdW50O18hSXRlbUNvdW50O18hRGF0YVNvdXJjZUl0ZW1Db3VudDtEYXRhS2V5czs+O2w8aTwxPjtpPDA+O2k8MD47bDw+Oz4+Oz47Ozs7Ozs7Ozs7Pjs7Pjt0PEAwPHA8cDxsPFBhZ2VDb3VudDtfIUl0ZW1Db3VudDtfIURhdGFTb3VyY2VJdGVtQ291bnQ7RGF0YUtleXM7PjtsPGk8MT47aTwzPjtpPDM+O2w8Pjs+Pjs+Ozs7Ozs7Ozs7Oz47bDxpPDA+Oz47bDx0PDtsPGk8MT47aTwyPjtpPDM+Oz47bDx0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDE0LTIwMTU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOW9ouWKv+S4juaUv+etlu+8iOWFre+8iTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w86YK55YmR5rOiOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwwLjU7Pj47Pjs7Pjs+Pjt0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDE0LTIwMTU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOmdmeaAgee9keermeiuvuiuoTs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w85pyx5pyd5bmzOz4+Oz47Oz47dDxwPHA8bDxUZXh0Oz47bDwxLjA7Pj47Pjs7Pjs+Pjt0PDtsPGk8MD47aTwxPjtpPDI+O2k8Mz47aTw0Pjs+O2w8dDxwPHA8bDxUZXh0Oz47bDwyMDE0LTIwMTU7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPDI7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPEFTUC5uZXTnvZHnu5znvJbnqIvpobnnm67lrp7ot7U7Pj47Pjs7Pjt0PHA8cDxsPFRleHQ7PjtsPOW7lueOsjs+Pjs+Ozs+O3Q8cDxwPGw8VGV4dDs+O2w8Mi4wOz4+Oz47Oz47Pj47Pj47Pj47Pj47Pj47PkJTN1Tin34ayK0cVqfm9N+xd+oE";
            //string xnd = grade + "-" + (grade + 1);
            string xnd = "2014-2015";
            string xqd = "1";

            String dataStr = string.Format(
                "__EVENTTARGET={0}&__EVENTARGUMENT={1}&__VIEWSTATE={2}&xnd={3}&xqd={4}"
                , eventtarget
                , eventargument
                , viewstate
                , xnd
                , xqd);
            string result = webHandler.Post(rootPath + "/xskbcx.aspx?xh=" + xh + "&gnmkdm=N121603", dataStr);
            Debug.WriteLine(result);
            if (result == null) return null;
            return result;
        }

        //获取验证码图片
        public ImageSource GetCheckCode()
        {
            return webHandler.GetImgToBitmapImage(rootPath + "/CheckCode.aspx");
        }
    }
}
