﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MineBoxRelease.Core.View;
using MineBoxRelease.Core.View.Creator;
using MineBoxRelease.View;

namespace MineBoxRelease
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ViewManager.RegisterView("AppBox", typeof(AppBoxView), "\xe606", typeof(AppBoxViewCreator));
            ViewManager.RegisterView("Schedule", typeof(ScheduleView), "\xe607", typeof(ScheduleViewCreator));
            ViewManager.RegisterView("Notepad", typeof(NotepadView), "\xe609", typeof(NotepadViewCreator));
            ViewManager.RegisterView("PlanBox", typeof(PlanBoxView), "\xe608", typeof(PlanBoxViewCreator));
            ViewManager.RegisterView("MusicBox", typeof(MusicBoxView), "\xe60a", typeof(MusicBoxViewCreator));
        }
    }
}
