﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Core.View;

namespace MineBoxRelease.View
{
    public class ViewCreatedEventArgs : RoutedEventArgs
    {
        public ViewCreatedEventArgs(RoutedEvent routedEvent, object source) : base(routedEvent, source) { }
        public String ViewName { get; set; }
        public String Title { get; set; }
    }

    /// <summary>
    /// DefaultView.xaml 的交互逻辑
    /// </summary>
    public partial class DefaultView : BaseView
    {
        public DefaultView()
        {
            InitializeComponent();
        }

        //注册事件
        public static readonly RoutedEvent ViewCreatedEvent =
            EventManager.RegisterRoutedEvent("ViewCreated", RoutingStrategy.Bubble, typeof(EventHandler<ViewCreatedEventArgs>), typeof(DefaultView));

        //定义事件处理器属性
        public event EventHandler<ViewCreatedEventArgs> ViewCreated
        {
            add { AddHandler(ViewCreatedEvent, value); }
            remove { RemoveHandler(ViewCreatedEvent, value); }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn == null || btn.Tag.Equals("")) return;
            var args = new ViewCreatedEventArgs(ViewCreatedEvent, this);
            args.ViewName = btn.Tag.ToString();
            args.Title = tbTitle.Text;
            RaiseEvent(args);
        }
    }
}
