﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using MineBoxRelease.Custom;

namespace MineBoxRelease.View
{
    public class BaseView : UserControl
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DefaultTabItem Tab { get; set; }

        public BaseView()
        {
            Id = -1;
        }
    }
}
