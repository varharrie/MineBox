﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Core.View;

namespace MineBoxRelease.View
{
    /// <summary>
    /// NotepadView.xaml 的交互逻辑
    /// </summary>
    public partial class NotepadView : BaseView
    {
        public string NotepadContent
        {
            get { return tbContent.Text; }
            set { tbContent.Text = value; }
        }

        public NotepadView(string title = null)
        {
            InitializeComponent();
            InitializeData();
            InitializeEvent();
            Title = title;
            if (title != null)
                tbTitle.Text = title;
        }
        //初始化变量
        private void InitializeData()
        {
        }

        //初始化事件
        private void InitializeEvent()
        {
            btnSaveAs.Click += (s, e) =>
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "文本文件(*.txt)|*.txt";
                if (dialog.ShowDialog() != DialogResult.OK || dialog.FileName == null || dialog.FileName.Equals("")) return;
                FileStream stream = new FileStream(dialog.FileName, FileMode.Create);
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(tbContent.Text);
                writer.Close();
                stream.Close();
            };
            tbContent.LostFocus += (s, e) => ViewManager.Save(this);
        }
    }
}
