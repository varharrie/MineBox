﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Core.View;
using MineBoxRelease.Custom;
using MineBoxRelease.Model;

namespace MineBoxRelease.View
{
    /// <summary>
    /// PlanBoxView.xaml 的交互逻辑
    /// </summary>
    public partial class PlanBoxView : BaseView
    {
        public PlanBoxView(string title = null)
        {
            InitializeComponent();
            InitializeData();
            InitializeEvent();
            Title = title;
            if (title != null)
                tbTitle.Text = title;
        }

        //初始化变量
        private void InitializeData()
        {
        }

        //初始化事件
        private void InitializeEvent()
        {
            btnAdd.Click += btnAdd_Click;
            btnClear.Click += btnClear_Click;
        }

        void btnClear_Click(object sender, RoutedEventArgs e)
        {
            List<PlanItem> removeingItems = new List<PlanItem>();
            foreach (var child in pnPlans.Children)
            {
                PlanItem item = child as PlanItem;
                if (item == null || !item.Plan.IsCompleted) continue;
                removeingItems.Add(item);
            }
            foreach (PlanItem item in removeingItems)
            {
                pnPlans.Children.Remove(item);
            }
            ViewManager.Save(this);
        }

        void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            PlanItem item = new PlanItem(new Plan());
            pnPlans.Children.Insert(0, item);
            pnPlans.UpdateLayout();
            item.DoFocus();
        }

        public List<Plan> GetList()
        {
            List<Plan> list = new List<Plan>();
            foreach (var child in pnPlans.Children)
            {
                PlanItem item = child as PlanItem;
                if (item == null) continue;
                list.Add(item.Plan);
            }
            return list;
        }

        public void SetList(List<Plan> list)
        {
            foreach (Plan plan in list)
            {
                PlanItem item = new PlanItem(plan);
                pnPlans.Children.Add(item);
            }
        }

        private void PlanItem_OnCheckChanged(object sender, RoutedEventArgs e)
        {
            PlanItem item = sender as PlanItem;
            if (item == null) return;
            if (item.Plan.IsCompleted)
            {
                pnPlans.Children.Remove(item);
                pnPlans.Children.Add(item);
            }
            else
            {
                pnPlans.Children.Remove(item);
                pnPlans.Children.Insert(0, item);
            }
        }

        private void PlanItem_OnEnterAccepted(object sender, RoutedEventArgs e)
        {
            PlanItem item = new PlanItem(new Plan());
            pnPlans.Children.Insert(0, item);
            pnPlans.UpdateLayout();
            item.DoFocus();
        }

        private void PlanItem_OnInputFinished(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("save!");
            ViewManager.Save(this);
        }

        private void PlanItem_OnClosing(object sender, RoutedEventArgs e)
        {
            PlanItem item = sender as PlanItem;
            if (item == null) return;
            pnPlans.Children.Remove(item);
            ViewManager.Save(this);
        }
    }
}
