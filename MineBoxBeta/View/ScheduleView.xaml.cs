﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MineBoxRelease.Core;
using MineBoxRelease.Core.View;
using MineBoxRelease.Custom;
using MineBoxRelease.Model;
using MineBoxRelease.Win;

namespace MineBoxRelease.View
{
    /// <summary>
    /// ScheduleView.xaml 的交互逻辑
    /// </summary>
    public partial class ScheduleView : BaseView
    {
        public int CurWeek { get; set; }
        public int Week { get; set; }
        public CourseSet Courses { get; set; }
        public ScheduleView(string title = null)
        {
            InitializeComponent();
            InitializeData();
            InitializeEvent();
            Title = title;
            if (title != null)
                tbTitle.Text = title;
        }

        //初始化变量
        private void InitializeData()
        {
            CurWeek = 1;
            Week = CurWeek;
        }

        //初始化事件
        private void InitializeEvent()
        {
            btnImport.Click += (s, e) => Import();
            btnSetWeek.MouseEnter += (s, e) => btnSetWeek.Content = "设置为当前周";
            btnSetWeek.MouseLeave += (s, e) => btnSetWeek.Content = "第" + Week + "周" + ((Week == CurWeek) ? "（当前）" : "");
            btnSetWeek.Click += (s, e) =>
            {
                CurWeek = Week;
                ViewManager.Save(this);
                btnSetWeek.Content = "设置成功";
            };
            btnPre.Click += (s, e) => GetWeekCourses(Week - 1);
            btnNext.Click += (s, e) => GetWeekCourses(Week + 1);
        }

        //导入课程表
        void Import()
        {
            ScheduleImportWindow win = new ScheduleImportWindow();
            if (win.ShowDialog() == true)
            {
                Courses = win.Courses;
                GetWeekCourses(CurWeek);
                ViewManager.Save(this);
            }
        }

        public void GetWeekCourses(int week)
        {
            if (week < 1 || week > 20) return;
            Week = week;
            btnSetWeek.Content = "第" + Week + "周" + ((Week == CurWeek) ? "（当前）" : "");
            pnCourses.Children.Clear();
            for (int row = 0; row < 6; row++)
            {
                for (int col = 0; col < 5; col++)
                {
                    pnCourses.Children.Add(new CourseItem(Courses.GetCourse(week, col, row)));
                }
            }
            gdShow.Visibility = Visibility.Visible;
            gdImport.Visibility = Visibility.Collapsed;
        }
    }
}
